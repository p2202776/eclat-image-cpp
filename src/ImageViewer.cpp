#include "ImageViewer.h"
#include <SDL.h>

ImageViewer::ImageViewer() : window(nullptr), renderer(nullptr) {
    // Initialisation de SDL2
    SDL_Init(SDL_INIT_VIDEO);

    // Création de la fenêtre SDL
    window = SDL_CreateWindow("Image Viewer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 200, 200, SDL_WINDOW_SHOWN);
    if (window == nullptr) {
        SDL_Log("Failed to create window: %s", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    // Création du renderer SDL
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == nullptr) {
        SDL_Log("Failed to create renderer: %s", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

}

ImageViewer::~ImageViewer() {
    // Libére le renderer et la fenêtre SDL
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    // Quitte SDL
    SDL_Quit();
}

void ImageViewer::afficher(const Image& im) {
    // Variables pour le zoom et la fermeture de la fenêtre
    bool isOpen = true;
    float zoom = 1.0;
    SDL_Event event;

    // Boucle principale pour afficher l'image et gérer les événements
    while (isOpen) {
        // Efface l'écran avec un fond gris clair
        SDL_SetRenderDrawColor(renderer, 200, 200, 200, SDL_ALPHA_OPAQUE);
        SDL_RenderClear(renderer);

        // Dessine l'image en bouclant sur chaque pixel
        for (int y = 0; y < im.getDimy(); ++y) {
            for (int x = 0; x < im.getDimx(); ++x) {
                Pixel pixel = im.getPix(x, y);
                // Dessine un rectangle pour chaque pixel avec la couleur correspondante
                SDL_Rect rect = {static_cast<int>(x * zoom), static_cast<int>(y * zoom), static_cast<int>(zoom),
                                 static_cast<int>(zoom)}; // static_cast<int> converti float to int
                SDL_SetRenderDrawColor(renderer, pixel.r, pixel.g, pixel.b, SDL_ALPHA_OPAQUE);
                SDL_RenderFillRect(renderer, &rect);
            }
        }

        // Affiche le rendu
        SDL_RenderPresent(renderer);

        // Gére les événements
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) { // Si l'utilisateur ferme la fenêtre
                isOpen = false;
            } else if (event.type == SDL_KEYDOWN) { // Si une touche est enfoncée
                switch (event.key.keysym.sym) {
                    case SDLK_ESCAPE: // Touche ESCAPE pour quitter
                        isOpen = false;
                        break;
                    case SDLK_t: // Touche T pour zoomer
                        zoom += 0.1;
                        break;
                    case SDLK_g: // Touche G pour dézoomer
                        zoom -= 0.1;
                        if (zoom < 0.1) zoom = 0.1;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
