cmake_minimum_required(VERSION 3.0)
project(12201052_12018792_12202776)

set(CMAKE_CXX_STANDARD 17)
set(EXAMPLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
set(TEST_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
set(AFFICHAGE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)

# Définir le chemin d'installation de SDL2 s'il n'est pas déjà défini
# set(SDL2_DIR "path_to_your_SDL2_installation")
set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/build)
set(SDL2_PATH "D:/SDL2-2.30.0/x86_64-w64-mingw32")
find_package(SDL2 REQUIRED)
include_directories(${SDL2_INCLUDE_DIR})

find_package(SDL2 REQUIRED)

include_directories(.)
include_directories(src)

add_executable(test src/mainTest.cpp
        src/Image.h
        src/Pixel.h
        src/Image.cpp
        src/Pixel.cpp
)
add_executable(exemple src/mainExemple.cpp
        src/Image.h
        src/Image.cpp
        src/Pixel.h
        src/Pixel.cpp
)
add_executable(affichage src/mainAffichage.cpp
        src/ImageViewer.h
        src/ImageViewer.cpp
        src/Image.h
        src/Image.cpp
        src/Pixel.h
        src/Pixel.cpp
)

# Définition du chemin de sortie pour l'exécutable exemple
set_target_properties(exemple PROPERTIES
        RUNTIME_OUTPUT_DIRECTORY ${EXAMPLE_OUTPUT_PATH}
)

set_target_properties(test PROPERTIES
        RUNTIME_OUTPUT_DIRECTORY ${TEST_OUTPUT_PATH}
)

set_target_properties(affichage PROPERTIES
        RUNTIME_OUTPUT_DIRECTORY ${AFFICHAGE_OUTPUT_PATH}
)


target_link_libraries(affichage ${SDL2_LIBRARY})